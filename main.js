// UI Icons

/* Filled */
import User from './components/filled/User.vue'
import Plus from './components/filled/Plus.vue'
import Additional from './components/filled/Additional.vue'
import Filter from './components/filled/Filter.vue'
import Globe from './components/filled/Globe.vue'
import GlobeAlt from './components/filled/GlobeAlt.vue'
import Phone from './components/filled/Phone.vue'
import Pencil from './components/filled/Pencil.vue'
import Trash from './components/filled/Trash.vue'
import Check from './components/filled/Check.vue'
import Times from './components/filled/Times.vue'
import Clock from './components/filled/Clock.vue'
import Refresh from './components/filled/Refresh.vue'
import Calendar from './components/filled/Calendar.vue'
import Cash from './components/filled/Cash.vue'
import Collection from './components/filled/Collection.vue'
import Ticket from './components/filled/Ticket.vue'
import Photograph from './components/filled/Photograph.vue'
import Mail from './components/filled/Mail.vue'
import Cube from './components/filled/Cube.vue'
import Code from './components/filled/Code.vue'
import Router from './components/filled/Router.vue'
import Map from './components/filled/Map.vue'
import Search from './components/filled/Search.vue'
import Cart from './components/filled/Cart.vue'
import Bookmark from './components/filled/Bookmark.vue'
import Inbox from './components/filled/Inbox.vue'
import Bell from './components/filled/Bell.vue'
import Key from './components/filled/Key.vue'
import Folder from './components/filled/Folder.vue'
import Link from './components/filled/Link.vue'
import Identification from './components/filled/Identification.vue'
import Truck from './components/filled/Truck.vue'
import Chip from './components/filled/Chip.vue'
import Qrcode from './components/filled/Qrcode.vue'
import Exclamation from './components/filled/Exclamation.vue'
import Flag from './components/filled/Flag.vue'
import Play from './components/filled/Play.vue'
import Pause from './components/filled/Pause.vue'
import Puzzle from './components/filled/Puzzle.vue'
import Eye from './components/filled/Eye.vue'
import Briefcase from './components/filled/Briefcase.vue'
import Backspace from './components/filled/Backspace.vue'
import Compass from './components/filled/Compass.vue'
import Menu from './components/filled/Menu.vue'
import Coverage from './components/filled/Coverage.vue'
import Battery from './components/filled/Battery.vue'
import Cog from './components/filled/Cog.vue'
import Hashtag from './components/filled/Hashtag.vue'
import Grounding from './components/filled/Grounding.vue'
import Server from './components/filled/Server.vue'
import Reply from './components/filled/Reply.vue'
import Selector from './components/filled/Selector.vue'
import Telephone from './components/filled/Telephone.vue'
import DeviceMobile from './components/filled/DeviceMobile.vue'
import DeviceDesktop from './components/filled/DeviceDesktop.vue'

import AppsGrid from './components/filled/AppsGrid.vue'
import ClipboardList from './components/filled/ClipboardList.vue'
import ClipboardCheck from './components/filled/ClipboardCheck.vue'
import UserGroup from './components/filled/UserGroup.vue'
import ChevronUp from './components/filled/ChevronUp.vue'
import ChevronDown from './components/filled/ChevronDown.vue'
import ChevronRight from './components/filled/ChevronRight.vue'
import ChevronLeft from './components/filled/ChevronLeft.vue'
import ChevronDoubleRight from './components/filled/ChevronDoubleRight.vue'
import ReceiptTax from './components/filled/ReceiptTax.vue'
import ViewGridAdd from './components/filled/ViewGridAdd.vue'
import LocationMarker from './components/filled/LocationMarker.vue'
import CubeTransparent from './components/filled/CubeTransparent.vue'
import ExternalLink from './components/filled/ExternalLink.vue'
import ColorSwatch from './components/filled/ColorSwatch.vue'
import StatusOnline from './components/filled/StatusOnline.vue'
import StatusOffline from './components/filled/StatusOffline.vue'
import BadgeCheck from './components/filled/BadgeCheck.vue'
import ShieldCheck from './components/filled/ShieldCheck.vue'
import CreditCard from './components/filled/CreditCard.vue'
import ShoppingBag from './components/filled/ShoppingBag.vue'
import PaperAirplane from './components/filled/PaperAirplane.vue'
import ArrowNarrowRight from './components/filled/ArrowNarrowRight.vue'
import ArrowDown from './components/filled/ArrowDown.vue'
import CheckCircle from './components/filled/CheckCircle.vue'
import PlusCircle from './components/filled/PlusCircle.vue'
import DotsHorizontal from './components/filled/DotsHorizontal.vue'
import DocumentDuplicate from './components/filled/DocumentDuplicate.vue'
import DocumentReport from './components/filled/DocumentReport.vue'
import LightningBolt from './components/filled/LightningBolt.vue'
import PencilAlt from './components/filled/PencilAlt.vue'
import EmojiHappy from './components/filled/EmojiHappy.vue'
import EmojiSad from './components/filled/EmojiSad.vue'
import CloudUpload from './components/filled/CloudUpload.vue'
import SwitchVertical from './components/filled/SwitchVertical.vue'
import ThumbUp from './components/filled/ThumbUp.vue'
import ThumbDown from './components/filled/ThumbDown.vue'
import ViewList from './components/filled/ViewList.vue'
import CurrencyEuro from './components/filled/CurrencyEuro.vue'
import AtSymbol from './components/filled/AtSymbol.vue'
import PhoneMissedCall from './components/filled/PhoneMissedCall.vue'

/* Outlined */
import ArrowCircleUp from './components/outlined/ArrowCircleUp.vue'
import ArrowCircleDown from './components/outlined/ArrowCircleDown.vue'
import CheckCircleOutlined from './components/outlined/CheckCircle.vue'
import InformationCircle from './components/outlined/InformationCircle.vue'
import MinusCircle from './components/outlined/MinusCircle.vue'
import CalendarOutlined from './components/outlined/Calendar.vue'
import ClockOutlined from './components/outlined/Clock.vue'
import TicketOutlined from './components/outlined/Ticket.vue'
import CubeOutlined from './components/outlined/Cube.vue'
import CubeTransparentOutlined from './components/outlined/CubeTransparent.vue'
import ReceiptRefundOutlined from './components/outlined/ReceiptRefund.vue'
import SignatureOutlined from './components/outlined/Signature.vue'
import BranchOutlined from './components/outlined/Branch.vue'
import ServerOutlined from './components/outlined/Server.vue'
import TerminalOutlined from './components/outlined/Terminal.vue'
import ExclamationOutlined from './components/outlined/Exclamation.vue'
import ContractOutlined from './components/outlined/Contract.vue'
import CommitOutlined from './components/outlined/Commit.vue'

/* Logos */
import Nibble from './components/logos/Nibble.vue'
import Nod from './components/logos/Nod.vue'
import Mastercard from './components/logos/Mastercard.vue'
import Visa from './components/logos/Visa.vue'
import AmericanExpress from './components/logos/AmericanExpress.vue'
import Stripe from './components/logos/Stripe.vue'

/* Animated */
import Menu2Times from './components/animated/Menu2Times.vue'
import MiniSpinner from './components/animated/MiniSpinner.vue'
import MicroSpinner from './components/animated/MicroSpinner.vue'

const Icons = {
    install(Vue) {
        /* Filled */
        Vue.component('i-user', User);
        Vue.component('i-plus', Plus);
        Vue.component('i-additional', Additional);
        Vue.component('i-filter', Filter);
        Vue.component('i-globe', Globe);
        Vue.component('i-globe-alt', GlobeAlt);
        Vue.component('i-phone', Phone);
        Vue.component('i-pencil', Pencil);
        Vue.component('i-trash', Trash);
        Vue.component('i-check', Check);
        Vue.component('i-times', Times);
        Vue.component('i-clock', Clock);
        Vue.component('i-refresh', Refresh);
        Vue.component('i-calendar', Calendar);
        Vue.component('i-cash', Cash);
        Vue.component('i-collection', Collection);
        Vue.component('i-ticket', Ticket);
        Vue.component('i-photograph', Photograph);
        Vue.component('i-mail', Mail);
        Vue.component('i-cube', Cube);
        Vue.component('i-code', Code);
        Vue.component('i-router', Router);
        Vue.component('i-map', Map);
        Vue.component('i-search', Search);
        Vue.component('i-cart', Cart);
        Vue.component('i-bookmark', Bookmark);
        Vue.component('i-inbox', Inbox);
        Vue.component('i-bell', Bell);
        Vue.component('i-key', Key);
        Vue.component('i-folder', Folder);
        Vue.component('i-link', Link);
        Vue.component('i-identification', Identification);
        Vue.component('i-truck', Truck);
        Vue.component('i-chip', Chip);
        Vue.component('i-qrcode', Qrcode);
        Vue.component('i-exclamation', Exclamation);
        Vue.component('i-flag', Flag);
        Vue.component('i-play', Play);
        Vue.component('i-pause', Pause);
        Vue.component('i-puzzle', Puzzle);
        Vue.component('i-eye', Eye);
        Vue.component('i-briefcase', Briefcase);
        Vue.component('i-backspace', Backspace);
        Vue.component('i-compass', Compass);
        Vue.component('i-menu', Menu);
        Vue.component('i-coverage', Coverage);
        Vue.component('i-battery', Battery);
        Vue.component('i-cog', Cog);
        Vue.component('i-hashtag', Hashtag);
        Vue.component('i-grounding', Grounding);
        Vue.component('i-server', Server);
        Vue.component('i-reply', Reply);
        Vue.component('i-selector', Selector);
        Vue.component('i-telephone', Telephone);
        Vue.component('i-device-mobile', DeviceMobile);
        Vue.component('i-device-desktop', DeviceDesktop);
        Vue.component('i-currency-euro', CurrencyEuro);
        Vue.component('i-at-symbol', AtSymbol);
        Vue.component('i-phone-missed-call', PhoneMissedCall);

        Vue.component('i-apps-grid', AppsGrid);
        Vue.component('i-clipboard-list', ClipboardList);
        Vue.component('i-clipboard-check', ClipboardCheck);
        Vue.component('i-user-group', UserGroup);
        Vue.component('i-chevron-up', ChevronUp);
        Vue.component('i-chevron-down', ChevronDown);
        Vue.component('i-chevron-right', ChevronRight);
        Vue.component('i-chevron-left', ChevronLeft);
        Vue.component('i-chevron-double-right', ChevronDoubleRight);
        Vue.component('i-receipt-tax', ReceiptTax);
        Vue.component('i-view-grid-add', ViewGridAdd);
        Vue.component('i-location-marker', LocationMarker);
        Vue.component('i-cube-transparent', CubeTransparent);
        Vue.component('i-external-link', ExternalLink);
        Vue.component('i-color-swatch', ColorSwatch);
        Vue.component('i-status-online', StatusOnline);
        Vue.component('i-status-offline', StatusOffline);
        Vue.component('i-badge-check', BadgeCheck);
        Vue.component('i-shield-check', ShieldCheck);
        Vue.component('i-credit-card', CreditCard);
        Vue.component('i-shopping-bag', ShoppingBag);
        Vue.component('i-paper-airplane', PaperAirplane);
        Vue.component('i-arrow-narrow-right', ArrowNarrowRight);
        Vue.component('i-arrow-down', ArrowDown);
        Vue.component('i-check-circle', CheckCircle);
        Vue.component('i-plus-circle', PlusCircle);
        Vue.component('i-dots-horizontal', DotsHorizontal);
        Vue.component('i-document-duplicate', DocumentDuplicate);
        Vue.component('i-document-report', DocumentReport);
        Vue.component('i-lightning-bolt', LightningBolt);
        Vue.component('i-pencil-alt', PencilAlt);
        Vue.component('i-emoji-happy', EmojiHappy);
        Vue.component('i-emoji-sad', EmojiSad);
        Vue.component('i-cloud-upload', CloudUpload);
        Vue.component('i-switch-vertical', SwitchVertical);
        Vue.component('i-thumb-up', ThumbUp);
        Vue.component('i-thumb-down', ThumbDown);
        Vue.component('i-view-list', ViewList);

        /* Outlined */
        Vue.component('i-arrow-circle-up', ArrowCircleUp);
        Vue.component('i-arrow-circle-down', ArrowCircleDown);
        Vue.component('i-check-circle-o', CheckCircleOutlined);
        Vue.component('i-information-circle', InformationCircle);
        Vue.component('i-minus-circle', MinusCircle);
        Vue.component('i-calendar-o', CalendarOutlined);
        Vue.component('i-clock-o', ClockOutlined);
        Vue.component('i-ticket-o', TicketOutlined);
        Vue.component('i-cube-o', CubeOutlined);
        Vue.component('i-cube-transparent-o', CubeTransparentOutlined);
        Vue.component('i-receipt-refund-o', ReceiptRefundOutlined);
        Vue.component('i-signature-o', SignatureOutlined);
        Vue.component('i-branch-o', BranchOutlined);
        Vue.component('i-server-o', ServerOutlined);
        Vue.component('i-terminal-o', TerminalOutlined);
        Vue.component('i-exclamation-o', ExclamationOutlined);
        Vue.component('i-contract-o', ContractOutlined);
        Vue.component('i-commit-o', CommitOutlined);


        /* Logos */
        Vue.component('i-nibble', Nibble);
        Vue.component('i-nod', Nod);
        Vue.component('i-card-mastercard', Mastercard);
        Vue.component('i-card-visa', Visa);
        Vue.component('i-card-american-express', AmericanExpress);
        Vue.component('i-stripe', Stripe);

        /* Animated */
        Vue.component('i-menu-to-times', Menu2Times);
        Vue.component('i-mini-spinner', MiniSpinner);
        Vue.component('i-micro-spinner', MicroSpinner);
    }
};

export default Icons;
